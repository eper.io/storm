
# storm

Storm is a straightforward load balancer implementation.

Content providers can use remote loading as a proxy. They can also provide content with polling for a shard. This helps to handle expensive GPU based hardware more efficiently.

## Shards and polling

Reference: CC41212D-234F-4EFD-967A-5ED227CC8B10

We use sharding to distribute requests. This allows to use 2, 10 or 100 servers to handle requests. Shards also poll serialized, so they can solve synchronization issues. The shard count is a semaphore protecting the network from overloading in this case.

Shards are nodes where the call belongs to.
Shard is a function of an attribute. This can be a path, or the content body.
If neither of them gives a shard we will use a random selected one.
All shards must be working

## Logged APIs

Reference: C3480898-A0A7-457F-AFEE-2014D419C666

Logged application programming interfaces follow the pattern used in Kubernetes or Docker.

Any call is sent directly to an in memory storage. Only minimum sanity checks are performed.
The user can request a logged call with `?logged=1` in the search parameter.
Logged calls are very helpful to maintain responsiveness with big calls that require longer processing.
We sacrifice some very low latencies with quick calls, so we give it as an option.